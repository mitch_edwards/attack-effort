# -*- coding: utf-8 -*-
"""CAL API client including authentication."""
# standard library
import base64
import hashlib
import hmac
import time
from typing import Dict, List
# third-party
from requests import PreparedRequest, Session
from requests.auth import AuthBase
from tcex.sessions import ExternalSession
# first-party
from cal_sdk.cal_types import CALDetailsResponse

class CALAuth(AuthBase):
    """Token-based auth for CAL."""

    def __init__(self, token: str, timestamp: str):
        """Token authentication for CAL.
​
        Args:
            token: authorization token from CAL.
            timestamp: token timestamp.
        """
        self.token = token
        self.timestamp = timestamp
        
    def __call__(self, r: PreparedRequest):
        """Add authorization headers to a CAL request.

        Args:
            r: the request to be sent.
            
        Returns:
            Request with CAL auth headers added.
        """
        r.headers['Authorization'] = self.token
        r.headers['Timestamp'] = self.timestamp
        return r
    
def create_cal_token_and_timestamp(license_key: str, instance_id: str) -> CALAuth:
    """Create a CALAuth object from TC license info.
​
    Both of the arguments can be found in a TC license file.
    Args:
        license_key: License key from license xml file.
        instance_id: Instance ID from license xml file.
​
    Returns:
        CALAuth object.
    """
    tc_cal_timestamp = str(int(time.time()))

    signature = f'{instance_id}:{tc_cal_timestamp}'
    hmac_signature = hmac.new(
        license_key.encode(), signature.encode(), digestmod=hashlib.sha256
    ).digest()
    tc_cal_token = f'HELIXTOKEN {signature}:{base64.b64encode(hmac_signature).decode()}'

    return CALAuth(tc_cal_token, tc_cal_timestamp)


class GroundTruth:
    def __init__(self, text: str):
        self.text = text
        self.tags = []
        self.research_metadata = {}

    def add_tag(self, object_id: str, refs: List[str]):
        self.tags.append({
            'objectid': object_id,
            'refs': refs
        })

    def add_research_metadata(self, key: str, value: str):
        self.research_metadata[key] = value

    def to_json(self):
        return {
            'text': self.text,
            'tags': self.tags,
            'research_metadata': self.research_metadata
        }


class CAL:
    """Client for CAL API."""

    def __init__(
        self,
        cal_host: str,
        instance_id: str,
        license_key: str,
        verify_ssl=True,
    ):
        """SDK for the CAL API.

        Args:
            cal_host: CAL instance host.
            cal_token: authorization token from CAL.
            cal_timestamp: token timestamp.
            session: Session to use to communicate with CAL.
            verify_ssl: whether or not to verify CAL's SSL certificate.
        """
        self.host = cal_host
        self._session: Session = Session()
        self._session.verify = verify_ssl
        self._session.auth = create_cal_token_and_timestamp(license_key, instance_id)

    def get_details(self, indicators: List[Dict[str, str]]) -> List[CALDetailsResponse]:
        """Send indicators to CAL to get details.

        Args:
            indicators: Indicators to send to cal, expected to be in the format:
                {
                    'uniqueId': '127.0.0.1',
                    'indicatorType': 'Address'
                }

        Returns:
            A list of CALDetailResponses.

        Raises:
            HTTPError on a non-successful response from CAL.
        """
        response = self._session.post(
            '/helix/indicators/v4/details?source=playbooks',
            headers={'Content-Type': 'application/json', 'Cache-Control': 'no-cache'},
            json=indicators,
        )
        response.raise_for_status()

        return [CALDetailsResponse(d) for d in response.json()]

    def post_groundtruth(self, data: GroundTruth):
        response = self._session.post(
            f'{self.host}/helix/document/v1/groundtruth',
            json=data.to_json())
        response.raise_for_status()
        return response
