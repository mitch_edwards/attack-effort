# -*- coding: utf-8 -*-
"""Various types for handling CAL responses"""
# standard library
from typing import Dict, List


class CALDetail:
    """A detail object from a CAL details API response."""

    def __init__(self, raw_dict=None):
        """Representation of CAL Details API Detail object.

        Args:
            raw_dict: the details array from the API response, parsed.
        """
        if raw_dict is None:
            raw_dict = {}
        self.json = raw_dict
    
    @property
    def description(self):
        """Return the description field."""
        return self.json.get('description')
    
    @property
    def display_name(self):
        """Return the display_name field."""
        return self.json.get('display_name')
    
    @property
    def group(self):
        """Return the group field."""
        return self.json.get('group')
    
    @property
    def name(self):
        """Return the name field

        Returns:
            the "name" field.
        """
        return self.json.get('name')

    @property
    def value(self):
        """Return the value field

        Returns:
            the "value" field.
        """
        return self.json.get('value')


class CALListResponse:
    """Response from the CAL details API: details for a single indicator."""

    def __init__(self, raw_dict: Dict):
        """Representation of a response from the CAL details API.

        Args:
            raw_dict: the raw json response from CAL, parsed into a dict.
        """
        self._json = raw_dict

    def get_pivot(self, pivot_id):
        for pivot in self.pivots:
            if pivot.get('id') == pivot_id:
                return pivot
        return None

    @property
    def json(self):
        return self._json

    @property
    def pivots(self):
        return self.json.get('pivots')

    @property
    def actions(self):
        return self.json.get('actions')

    @property
    def entities(self):
        return self.json.get('entities')


class CALDetailsResponse:
    """Response from the CAL details API: details for a single indicator."""

    def __init__(self, raw_dict: Dict[str, int]):
        """Representation of a response from the CAL details API.

        Args:
            raw_dict: the raw json response from CAL, parsed into a dict.
        """
        self._json = raw_dict
        self._parsed_details = None

    @property
    def json(self):
        return self._json

    @property
    def unique_id(self):
        """Return the unique ID field

        Returns:
            the "uniqueId" field, which is the indicator value.
        """
        return self._json.get('uniqueId')

    @property
    def score(self):
        """Return the score field

        Returns:
            the "score" field
        """
        return self._json.get('score')

    @property
    def indicator_status(self):
        """Return the indicatorStatus field

        Returns:
            the "indicatorStatus" field.
        """
        return self._json.get('indicatorStatus')

    @property
    def good(self):
        """Returns the good field.

        Returns:
            the "good" field.
        """
        return self._json.get('good')

    @property
    def details(self) -> List[CALDetail]:
        """Return a list of details in this CAL response.

        Returns:
            A list of the details in this response, wrapped in CALDetail objects.
        """
        if self._parsed_details is None:
            self._parsed_details = list([CALDetail(d) for d in self._json.get('details', [])])

        return self._parsed_details


class CALRelationship:

    def __init__(self, raw_dict: Dict[str, int]):
        """Representation of a response from the CAL details API.

        Args:
            raw_dict: the raw json response from CAL, parsed into a dict.
        """
        self.json = raw_dict

    @property
    def relationship(self):
        """Return the relationship field

        Returns:
            the "relationship" field
        """
        return self.json.get('relationship')

    @property
    def unique_id(self):
        """Return the unique ID field

        Returns:
            the "uniqueId" field, which is the indicator value.
        """
        return self.json.get('uniqueId')

    @property
    def score(self):
        """Return the unique ID field

        Returns:
            the "score" field.
        """
        return self.json.get('score')

    @property
    def first_seen(self):
        """Return the first seen field

        Returns:
            the "firstSeen" field, in the format %Y-%m-%dT%H:%M:%S.000Z.
        """
        return self.json.get('firstSeen')

    @property
    def last_seen(self):
        """Return the last seen field

        Returns:
            the "lastSeen" field, in the format %Y-%m-%dT%H:%M:%S.000Z.
        """
        return self.json.get('lastSeen')

    @property
    def latest(self):
        """Return the latest ID field

        Returns:
            the "latest" field.
        """
        return self.json.get('latest')

    @property
    def count(self):
        """Return the count field

        Returns:
            the "count" field
        """
        return self.json.get('count')

    @property
    def indicator_type(self):
        """Return the indicatorType field

        Returns:
            the "indicatorType" field, which is the indicator type.
        """
        return self.json.get('indicatorType')


class CALDataResponse:
    """Response from the CAL details API: details for a single indicator."""

    def __init__(self, raw_dict: Dict[str, int]):
        """Representation of a response from the CAL details API.

        Args:
            raw_dict: the raw json response from CAL, parsed into a dict.
        """
        self.json = raw_dict

    @property
    def unique_id(self):
        """Return the unique ID field

        Returns:
            the "uniqueId" field, which is the indicator value.
        """
        return self.json.get('uniqueId')

    @property
    def truncated(self):
        """Return the truncated field

        Returns:
            the "truncated" field.
        """
        return self.json.get('truncated')

    @property
    def relationship(self):
        """Return the relationship field

        Returns:
            the "relationship" field.
        """
        return self.json.get('relationship')

    @property
    def relationship_count(self):
        """Return the relationship count field

        Returns:
            the "relationshipCount" field.
        """
        return self.json.get('relationshipCount')

    @property
    def indicator_type(self):
        """Return the indicatorType field

        Returns:
            the "indicatorType" field, which is the indicator type.
        """
        return self.json.get('indicatorType')

    @property
    def relationships(self):
        """Return the relationships field

        Returns:
            the "relationships" field, which is the indicator relationships.
        """
        return [CALRelationship(d) for d in self.json.get('relationships', [])]


class CALCountResponse:
    """Response from the CAL details API: details for a single indicator."""

    def __init__(self, raw_dict: Dict[str, int]):
        """Representation of a response from the CAL details API.

        Args:
            raw_dict: the raw json response from CAL, parsed into a dict.
        """
        self.json = raw_dict

    @property
    def unique_id(self):
        """Return the unique ID field

        Returns:
            the "uniqueId" field, which is the indicator value.
        """
        return self.json.get('uniqueId')

    @property
    def indicator_type(self):
        """Return the indicatorType field

        Returns:
            the "indicatorType" field, which is the indicator type.
        """
        return self.json.get('indicatorType')

    @property
    def relationships(self):
        """Return the relationships field

        Returns:
            the "relationships" field, which is the indicator relationships.
        """
        return [CALRelationship(d) for d in self.json.get('relationships', [])]


