
import requests

session = requests.session()
session.proxies = {}
session.proxies['http'] = 'socks5h://localhost:9050'
session.proxies['https'] = 'socks5h://localhost:9050'

headers = {}
headers['User-agent'] = "HotJava/1.1.2 FCS"

r = session.get('https://jasonrigden.com', headers=headers)
r2 = session.get('http://httpbin.org/ip', headers=headers)

print(r2.text)
