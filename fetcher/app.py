"""ThreatConnect Job App"""
# standard library
import csv
import json

# first-party
from external_app import ExternalApp  # Import default External App Class (Required)


class App(ExternalApp):
    """External App"""

    def __init__(self, _tcex: object):
        """Initialize class properties."""
        super().__init__(_tcex)
        self.batch = None
        self.url = 'https://feodotracker.abuse.ch/downloads/malware_hashes.csv'


    def run(self) -> None:

        """Run main App logic."""
        self.batch: object = self.tcex.batch(self.args.tc_owner)

        i = 0 # number of incidents in current bite
        num_total = 0 # total number of bites
        bites = [] # array of all bites

        # Pulls down all incidents from the Technical Blogs and Reports Organization
        parameters = {'includes': ['additional', 'attributes', 'labels', 'tags']}
        groups = self.tcex.ti.group(group_type='Incident', owner='Technical Blogs and Reports')
        bite = []

        bite_file_index = 1

        attack_keywords = {}

        with open('attack_keywords.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                id = row[0]
                name = row[1]
                link = row[3]
                keywords = row[4]
                attack_keywords[id] = {"name" : name, "link" : link, "keywords" : keywords.replace(" ","").split(",")}


        for group in groups.many(params=parameters):

            # check if 30 incidents have been parsed through


            # create incident json object and grab relevant attributes
            incident = {}
            incident["title"] = group['name']
            incident["date_created"] = group["dateAdded"] #grabs date Added
            incident["link"] = group["webLink"]

            name = group['name']
            link = group['webLink']
            try:
                for attr in group["attribute"]: # iterates through list of incident attributes
                    if attr["type"] == "Description":
                        incident["content"] = attr["value"]
                    elif attr["type"] == "Source":
                        incident["source"] = attr["value"]
            except:
                print(f'[-] Incident of name {name} at link {link} had no attributes')
            # Grab tags from incident and store them in an array
            tags = []
            try:
                for t in group["tag"]:
                    tags.append(t["name"])
            except:
                pass
            incident["tags"] = tags

            incident["mitre"] = {}
            mitre = {}

            try:
                for tech in attack_keywords:
                    mitre[tech] = []
                    keywords = attack_keywords[tech]["keywords"]
                    name = " " + attack_keywords[tech]["name"] + " "
                    if (name.upper() in incident["content"].upper()):
                        mitre[tech].append(name)
                    for word in keywords:
                        word_updated = " " + word + " "
                        if (word_updated.upper() in incident["content"].upper()):
                            mitre[tech].append(word)

                    if (len(mitre[tech]) > 0 and len(mitre[tech][0]) > 0):
                        incident["mitre"][tech] = ','.join([str(elem) for elem in mitre[tech]])

            except:
                pass

            i += 1


            bite.append(incident)
            if i == 30:
                i = 0
                num_total += 1

                f = open(f'./bites/bites_{bite_file_index}.json', 'w')
                dump = json.dumps(bite)
                f.write(dump)
                f.close()
                bite = []
                bite_file_index = bite_file_index+1
                bites = []


        # Convert bites array to JSON string and write to output file
        bites_json = json.dumps(bites)

        # self.exit_message = f'Downloaded and created {self.batch.indicator_len} file hashes.'
