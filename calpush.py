from cal_sdk.cal import CAL, GroundTruth


def writeToCal():
    instance_id = '7a14d30c-f0c0-4c27-ab49-bc4d6ef99542'
    license_key = '79fbd942-2971-4481-ba9c-840a474a5289'
    cal = CAL('https://helix-dev.threatconnect.com', instance_id, license_key)
    data = GroundTruth("This malware does bad things.  The master boot record is overwritten when it runs.  The MBR rewrite makes it hard to detect.  Then, the trojan rewrites the registry.  Overall, we don't know how it does this without raising alarms.")
    data.add_tag('T1484', ["master boot record is overwritten", "MBR rewrite"])
    data.add_tag('T1005', ["trojan rewrites the registry"])
    data.add_tag('T1569', [])
    data.add_research_metadata('comment', 'This is a test push, please ignore')
    res = cal.post_groundtruth(data)
    print(res.status_code)
    print(res.text)

    return res
