let div = document.getElementById('target-text')
let display = document.getElementById('target-text-p')
let content = document.getElementsByClassName('document-content')[0]
let title = document.getElementsByClassName('document-title')[0]
let buttons = document.getElementsByClassName('attack-button')
let submit = document.getElementById('submit-button')
let clear = document.getElementById('clear-button')
let tags = []
let filename = document.getElementById('filename').value
let index = document.getElementById('index').value
let bite = document.getElementById('bite').value

let picks = document.getElementById('picks').getElementsByTagName('ul')[0]
// console.log(buttons)

let objs = []


let selected = 'Select text...'
clear.addEventListener('click',e => {
    selected = 'Select text...';
    display.innerHTML = selected;
    e.preventDefault()
    window.getSelection().removeAllRanges();
});


for(i = 0; i < buttons.length; i++){
    buttons[i].addEventListener('click', (e) => {
        e.preventDefault()
        let obj = {};
        obj['tag'] = e.target.innerHTML;
        tags.push(e.target.innerHTML)
        if (selected === 'Select text...'){
            console.log('No text selected...')
            obj['tagged_text'] = '...'
        }else{
            obj['tagged_text'] = display.innerHTML;
        }
        obj['filename'] = filename
        obj['index'] = index
        console.log(obj)
        objs.push(obj)

        
        let li = document.createElement('LI')
        let text = document.createTextNode(obj['tag']+' - '+obj['tagged_text']+' - ')
        let button = document.createElement('button')
        button.class = 'btn-matrix remove'
        button.innerHTML = 'Remove tag'
        button.style.display = 'block'
        button.onclick = function(e) { 
            this.parentNode.parentNode.removeChild(this.parentNode) 
            newObjs = []
            for(let i = 0; i < objs.length; i++){
                console.log(text.wholeText.split(' - ')[1])
                console.log(objs[i]['tagged_text'])
                if(objs[i]['tagged_text'] === text.wholeText.split(' - ')[1]){
                   console.log('[-] Removed!')
                }else{
                    console.log('[-] Kept')
                    newObjs.push(objs[i])
                }
            }
            objs = newObjs
            console.log(objs)
        };
        
        li.appendChild(text)
        li.appendChild(button)
        picks.appendChild(li)
        removeListeners()
        console.log(objs)
    });
}

function removeParent(button){
    button.parentNode.parentNode.removeChild(button.parentNode)
}



submit.addEventListener('click', (e) => {
    console.log(objs)
    // Fetch the backend endpoint that handles this shit
    
    reqOptions = {
        'method':'POST',
        'mode':'no-cors',
        'headers':{'Content-Type':'application/json'},
        'body':JSON.stringify(objs)
    }
    fetch('http://127.0.0.1:5000/tag', reqOptions)

    

})


title.addEventListener('click', (e) =>{
    //console.log(e.target)
    selected = document.getSelection()
    // console.log(selected)

    div.value = selected
    display.innerHTML = document.getSelection()


})


content.addEventListener('click', (e) =>{
    //console.log(e.target)
    selected = document.getSelection()
    //console.log(selected)
    div.innerHTML = selected

    display.innerHTML = document.getSelection()
    div.value = selected
});

function removeListeners(){
    let removeButtons = picks.getElementsByClassName('remove')
    for(let i = 0; i < removeButtons.length; i++){
        removeButtons[i].addEventListener('click', button => {
            console.log('[-] Removed')
            button.parentNode.parentNode.removeChild(button.parentNode)
        });
    }
}